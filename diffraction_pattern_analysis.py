import cv2
import multiprocessing
import numpy as np
from PIL import Image
# from sys import stdout
from library import Library

# from time import time

class DiffractionPatternAnalysis:

    def __init__(self, adjustable_constants):

        self.constants = adjustable_constants

    def multiprocessing_func(self, vertical_range, horizontal_range, x_correction, y_correction,
                             radius_scaled_min, radius_scaled_max, data, all_output_values):
        result_data = dict()
        histogram_data = dict()
        analyzed_area_positions = list()
        separated_area_white = list()
        separated_area_dark = list()

        for vertical in vertical_range:
            # stdout.write('\r' + "[{}/{}] - {} - {} %".
            #              format(number_of_proceded_file, number_of_files, filename,
            #                     round((vertical - vertical_range_first_element + 1) /
            #                           vertical_length * 100, 1)))
            for horizontal in horizontal_range:

                x_distance = horizontal - x_correction
                y_distance = y_correction - vertical

                radius = np.sqrt(x_distance ** 2 + y_distance ** 2)

                if radius_scaled_min < radius < radius_scaled_max:

                    angle = np.arctan2(y_distance, x_distance) / np.pi * 180

                    angle = self.calculate_angle(angle)

                    if np.sign(angle) == -1:
                        angle += 360
                    if angle == 360:
                        angle = 0

                    angle = round(angle, 3)

                    value = int(data[vertical, horizontal])

                    try:
                        result_data[angle] += value
                    except KeyError:
                        result_data[angle] = value
                    try:
                        histogram_data[angle] += 1
                    except KeyError:
                        histogram_data[angle] = 1

                    analyzed_area_positions.append([vertical, horizontal, value])

                    angle = round(round(angle * 2 ** self.constants.precision, 0) /
                                  (2 ** self.constants.precision), 3)

                    if round(angle % ((1 / 2) ** (self.constants.precision - 1)), 3) == 0:
                        separated_area_white.append([vertical, horizontal])
                    else:
                        separated_area_dark.append([vertical, horizontal])

        all_output_values.append([result_data, histogram_data, analyzed_area_positions,
                                  separated_area_white, separated_area_dark])

    def calculate_angle(self, angle):
        return round(round(angle * self.constants.real_precision, 0) /
                     self.constants.real_precision, 3)

    def run(self):
        working_directory = "source_data"
        library = Library(working_directory)

        list_of_filenames = library.load_files()
        number_of_proceded_file = 0
        number_of_files = len(list_of_filenames)

        for filename in list_of_filenames:
            number_of_proceded_file += 1
            print("File name [{}/{}]: {}".format(number_of_proceded_file,
                                                 number_of_files, filename))
            library.make_dirs(filename)
            settings = library.load_settings(filename)
            if settings is not None:
                self.constants.central_position_offset_x = settings[0]
                self.constants.central_position_offset_y = settings[1]
                self.constants.radius_min = settings[2]
                self.constants.radius_max = settings[3]

            Image.MAX_IMAGE_PIXELS = 300000000
            image = Image.open("{}/{}".format(working_directory, filename))
            image = np.asarray(image)
            image_shape = image.shape

            data = np.asarray(image)[:image_shape[1]][:]

            library.save_picture(self.constants, image, "1_original_image")
            image = 0  # freeing up the memory

            original_shape = data.shape

            data = cv2.resize(data, dsize=(int(original_shape[1] / self.constants.scale),
                                           int(original_shape[0] / self.constants.scale)),
                              interpolation=cv2.INTER_CUBIC)

            data = np.copy(data)

            shape = data.shape
            print("Image resolution: {}".format(shape))

            manager = multiprocessing.Manager()
            all_output_values = manager.list()

            radius_scaled_min = self.constants.radius_min / self.constants.scale
            radius_scaled_max = self.constants.radius_max / self.constants.scale

            horizontal_range, vertical_range = self.get_horizontal_and_vertical_range(
                shape, radius_scaled_max)

            x_correction = (shape[1] / 2 + self.constants.central_position_offset_x /
                            self.constants.scale)
            y_correction = (shape[0] / 2 - self.constants.central_position_offset_y /
                            self.constants.scale)

            # vertical_length = len(list(vertical_range))
            # vertical_range_first_element = vertical_range[0]
            #
            # number_of_files = len(list_of_filenames)



            processes = []
            horizontal_list = list()

            try:
                threads = int(multiprocessing.cpu_count())
            except ValueError:
                threads = 8

            # threads = 6

            print("Threads used: {}".format(threads))

            step = len(horizontal_range) / threads

            n = 0
            for i in range(threads):
                n += 1
                first = int(step * (n - 1))
                second = int(step * n)
                horizontal_list.append(list(range(first, second)))

            print("Processing...")
            for horizontal_range in horizontal_list:
                p = multiprocessing.Process(target=self.multiprocessing_func,
                                            args=(vertical_range, horizontal_range, x_correction,
                                                  y_correction, radius_scaled_min,
                                                  radius_scaled_max, data, all_output_values))
                processes.append(p)
                p.start()

            for process in processes:
                process.join()

            print("Almost done...")

            (result_data, histogram_data, analyzed_area_positions, separated_area_white,
             separated_area_dark) = self.rearrange_output_data(all_output_values)

            analyzed_data = np.copy(data)
            analyzed_data = analyzed_data.astype(np.float16)
            analyzed_data *= 0.7
            analyzed_data = analyzed_data.astype(np.uint8)

            for position in analyzed_area_positions:
                analyzed_data[position[0], position[1]] = position[2]

            library.save_picture(self.constants, analyzed_data, "2_analyzed_area")
            analyzed_data = 0  # freeing up the memory

            separation_data = np.copy(data)

            for position in separated_area_dark:
                value = int(separation_data[position[0], position[1]])
                value -= 40
                if value < 0:
                    value = 0
                separation_data[position[0], position[1]] = value

            for position in separated_area_white:
                value = int(separation_data[position[0], position[1]])
                value += 40
                if value > 255:
                    value = 255
                separation_data[position[0], position[1]] = value

            library.save_picture(self.constants, separation_data, "3_highlighted_areas")
            separation_data = 0  # freeing up the memory

            result_value_ordered = list()
            result_value_ordered_cumulated = list()
            result_cumulated = dict()

            for i in range(0, 60 * 1000):
                i /= 1000
                i = round(i, 3)
                result_cumulated[i] = 0.0

            result_angle_ordered = list()
            number_of_pixels_in_area_ordered = list()

            for angle in range(360 * 1000):
                angle = angle / 1000
                angle = round(angle, 3)

                try:
                    value = result_data[angle] / 1000
                except KeyError:
                    continue

                if value == 0.0:
                    continue

                result_angle_ordered.append(angle)
                result_value_ordered.append(value)

                number_of_pixels_in_area_ordered.append(histogram_data[angle])

                if 30 <= angle < 330:
                    cumulated_angle = angle % 60
                    cumulated_angle = round(cumulated_angle, 3)
                    result_cumulated[cumulated_angle] += value

            result_angle_ordered_cumulated = list()
            for angle in result_cumulated:
                value = result_cumulated[angle]
                if value == 0.0:
                    continue
                result_angle_ordered_cumulated.append(angle)
                result_value_ordered_cumulated.append(value)

            library.save_pictures(self.constants, result_angle_ordered,
                                  result_value_ordered, number_of_pixels_in_area_ordered,
                                  result_angle_ordered_cumulated, result_value_ordered_cumulated)
            print("Done!")
        print("ALL DONE!")

    @staticmethod
    def rearrange_output_data(all_output_values):
        result_data_partial = list()
        histogram_data_partial = list()
        analyzed_area_positions_partial = list()
        separated_area_white_partial = list()
        separated_area_dark_partial = list()

        for process_output in all_output_values:
            result_data_partial.append(process_output[0])
            histogram_data_partial.append(process_output[1])
            analyzed_area_positions_partial.append(process_output[2])
            separated_area_white_partial.append(process_output[3])
            separated_area_dark_partial.append(process_output[4])

        result_data = dict()
        histogram_data = dict()
        analyzed_area_positions = list()
        separated_area_white = list()
        separated_area_dark = list()

        for single_output_list in result_data_partial:
            for key in single_output_list.keys():
                try:
                    result_data[key] += single_output_list[key]
                except KeyError:
                    result_data[key] = single_output_list[key]

        for single_output_list in histogram_data_partial:
            for key in single_output_list.keys():
                try:
                    histogram_data[key] += single_output_list[key]
                except KeyError:
                    histogram_data[key] = single_output_list[key]

        for single_output_list in analyzed_area_positions_partial:
            analyzed_area_positions += single_output_list

        for single_output_list in separated_area_white_partial:
            separated_area_white += single_output_list

        for single_output_list in separated_area_dark_partial:
            separated_area_dark += single_output_list

        return (result_data, histogram_data, analyzed_area_positions, separated_area_white,
                separated_area_dark)

    def get_horizontal_and_vertical_range(self, shape, radius_scaled_max):
        if self.constants.optimized_speed:
            horizontal_initial = int(shape[1] / 2 - radius_scaled_max +
                                     self.constants.central_position_offset_x /
                                     self.constants.scale)
            horizontal_final = int(shape[1] / 2 + radius_scaled_max +
                                   self.constants.central_position_offset_x /
                                   self.constants.scale) + 1

            vertical_initial = int(shape[0] / 2 - radius_scaled_max -
                                   self.constants.central_position_offset_y /
                                   self.constants.scale)
            vertical_final = int(shape[0] / 2 + radius_scaled_max -
                                 self.constants.central_position_offset_y /
                                 self.constants.scale) + 1

            horizontal_range = list(range(horizontal_initial, horizontal_final))
            vertical_range = list(range(vertical_initial, vertical_final))
        else:
            vertical_range = range(shape[0])
            horizontal_range = range(shape[1])

        return horizontal_range, vertical_range
