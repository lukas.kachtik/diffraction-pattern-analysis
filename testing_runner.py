from testing import processing


class AdjustableConstants:
    def __init__(self):
        self.scale = 1
        self.central_position_offset_x = -170
        self.central_position_offset_y = 20
        self.radius_min = 1280
        self.radius_max = 1400
        self.precision = 3
        self.optimized_speed = False
        self.real_precision = 2 ** self.precision

if __name__ == '__main__':


    proc = processing(AdjustableConstants())
    proc.run()

