from time import time
from diffraction_pattern_analysis import DiffractionPatternAnalysis


class AdjustableConstants:
    def __init__(self):
        self.scale = 1
        self.central_position_offset_x = -170
        self.central_position_offset_y = 20
        self.radius_min = 1280
        self.radius_max = 1400
        self.precision = 3
        self.optimized_speed = False
        self.real_precision = 2 ** self.precision
        self.save_as_tif = False
        self.save_as_png = True


if __name__ == '__main__':
    start_time = time()
    adjustable_constants = AdjustableConstants()
    diffraction_pattern_analysis = DiffractionPatternAnalysis(adjustable_constants)
    diffraction_pattern_analysis.run()
    print("Time: {} s".format(round(time() - start_time, 2)))
