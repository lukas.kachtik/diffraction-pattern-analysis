import matplotlib.pyplot as plt
import numpy as np
from os import listdir, path, makedirs
from os.path import isfile, join
from PIL import Image
import scipy.ndimage

class Library:

    def __init__(self, working_directory):
        self.results_folder_name = "results"
        self._working_directory = working_directory
        self._path = "not_defined"

    def load_files(self):
        loaded_files = [f for f in listdir(str(self._working_directory)) if isfile(join(str(
            self._working_directory), f))]
        return [file for file in loaded_files if file[-4:] in [".tif", "tiff"]]

    def load_settings(self, file_name):
        file_name = file_name[:-4]
        try:
            return np.loadtxt("{}/{}.txt".format(self._working_directory, file_name))
        except:
            return None

    def make_dirs(self, single_result_folder_name):
        if not path.exists(self.results_folder_name):
            makedirs(self.results_folder_name)
        self._path = '{}/{}'.format(self.results_folder_name, single_result_folder_name)
        if not path.exists(self._path):
            makedirs(self._path)

    def _get_noise_data(self, data):
        averaged_data = scipy.ndimage.filters.gaussian_filter1d(
            input=data, sigma=1, axis=-1, order=0, output=None,
            mode='reflect', cval=0.0,
            truncate=4.0)
        data_min = np.min(data)
        noise_data = data - averaged_data
        noise_data_max = np.max(noise_data)
        delta = (data_min - noise_data_max) * 0.98
        noise_data_to_draw = noise_data + delta
        return averaged_data, noise_data, noise_data_to_draw

    def save_picture(self, constants, image, name):
        file_extensions = []
        if constants.save_as_tif:
            file_extensions.append("tif")
        if constants.save_as_png:
            file_extensions.append("png")

        info_to_filename = "_s{}_p{}".format(constants.scale, constants.precision)

        for extension in file_extensions:
            Image.fromarray(image).save('{}/{}{}.{}'.format(
                self._path, name, info_to_filename, extension))

    def save_pictures(self, constants, result_angle_ordered,
                      result_value_ordered, number_of_pixels_in_area_ordered,
                      result_angle_ordered_cumulated, result_value_ordered_cumulated):

        info_to_filename = "_s{}_p{}".format(constants.scale, constants.precision)

        linewidth = 0.5
        delta_angle = ", \u0394\u03b1 = {}\u00B0".format(1 / constants.real_precision)

        averaged_data, noise_data, noise_data_to_draw = self._get_noise_data(result_value_ordered)
        averaged_data_cumulated, noise_data_cumulated, noise_data_to_draw_cumulated = (
            self._get_noise_data(result_value_ordered_cumulated))
        _, noise_data_histogram, _ = (self._get_noise_data(number_of_pixels_in_area_ordered))

        plt.figure()
        plt.plot(result_angle_ordered, result_value_ordered, linewidth=linewidth)
        plt.title("Angular dependent intensity{}".format(delta_angle))
        plt.legend(["Intensity"], loc="upper right")
        plt.xlabel('Angle [degrees]')
        plt.ylabel('Intensity [kcounts]')
        plt.savefig('{}/4_result_plot{}.png'.format(self._path, info_to_filename), dpi=300)
        plt.close()

        plt.figure()
        plt.plot(result_angle_ordered, result_value_ordered,
                 result_angle_ordered, averaged_data,
                 result_angle_ordered, noise_data_to_draw, linewidth=linewidth)
        plt.title("Angular dependent intensity{}".format(delta_angle))
        plt.legend(["Intensity", "Averaged intensity", "Subtracted noise"], loc="upper right")
        plt.xlabel('Angle [degrees]')
        plt.ylabel('Intensity [kcounts]')
        plt.savefig('{}/5_result_plot{}.png'.format(self._path, info_to_filename), dpi=300)
        plt.close()

        plt.figure()
        plt.plot(result_angle_ordered_cumulated, result_value_ordered_cumulated,
                 linewidth=linewidth)
        plt.title("Cumulative angular dependent intensity{}".format(delta_angle))
        plt.legend(["Intensity"], loc="upper right")
        plt.xlabel('Angle [degrees]')
        plt.ylabel('Intensity [kcounts]')
        plt.savefig('{}/6_result_plot_cumulated{}.png'.format(self._path, info_to_filename),
                    dpi=300)
        plt.close()

        plt.figure()
        plt.plot(result_angle_ordered_cumulated, result_value_ordered_cumulated,
                 result_angle_ordered_cumulated, averaged_data_cumulated,
                 result_angle_ordered_cumulated, noise_data_to_draw_cumulated,
                 linewidth=linewidth)
        plt.title("Cumulative angular dependent intensity{}".format(delta_angle))
        plt.legend(["Intensity", "Averaged intensity", "Subtracted noise"], loc="upper right")
        plt.xlabel('Angle [degrees]')
        plt.ylabel('Intensity [kcounts]')
        plt.savefig('{}/7_result_plot_cumulated{}.png'.format(self._path, info_to_filename),
                    dpi=300)
        plt.close()

        plt.figure()
        plt.plot(result_angle_ordered, number_of_pixels_in_area_ordered, linewidth=linewidth)
        plt.title("Distribution of pixels in each area{}".format(delta_angle))
        plt.xlabel('Angle [degrees]')
        plt.ylabel('Number of pixels')
        plt.savefig('{}/8_area_distribution{}.png'.format(self._path, info_to_filename), dpi=300)
        plt.close()

        result_data_numpy = np.asarray([result_angle_ordered_cumulated,
                                        result_value_ordered_cumulated]).transpose()
        np.savetxt('{}/9_result_data{}.txt'.format(self._path, info_to_filename),
                   result_data_numpy, fmt='%.3f')


        data_to_summarize_names = ["=== 360 degree range ===",
                                   "=== 60 degree range ===",
                                   "=== Distribution of pixels ==="]
        data_to_summarize = [result_value_ordered,
                             result_value_ordered_cumulated,
                             number_of_pixels_in_area_ordered]

        noise_data_list = [noise_data, noise_data_cumulated, noise_data_histogram]

        info_file_list_all = list()

        for i in range(len(data_to_summarize)):

            name = data_to_summarize_names[i]
            data = data_to_summarize[i]
            current_noise_data = noise_data_list[i]

            average = np.average(data)
            median = float(np.median(data))
            std = float(np.std(data))
            max_value = float(np.max(data))
            min_value = float(np.min(data))

            relative_std_average = std / average * 100
            relative_std_median = std / median * 100
            relative_max = max_value / median * 100 - 100
            relative_min = min_value / median * 100 - 100

            level_of_noise = float(np.std(current_noise_data)) / average * 100

            round_value = 2
            info_file_list = [name + "\n",
                              "Average: {} kcounts".format(round(average, round_value)),
                              "Median: {} kcounts".format(round(median, round_value)),
                              "Std: {} kcounts".format(round(std, round_value)),
                              "Relative Std (average): {} %".format(round(relative_std_average,
                                                                          round_value)),
                              "Relative Std (median): {} %".format(round(relative_std_median,
                                                                         round_value)),
                              "Relative max difference: {} %".format(round(relative_max,
                                                                         round_value)),
                              "Relative min difference: {} %".format(round(relative_min,
                                                                           round_value)),
                              "Level of noise: {} %".format(round(level_of_noise, round_value)),
                              "\n"]
            info_file_list_all += info_file_list

        file = open('{}/10_info{}.txt'.format(self._path, info_to_filename), "w")
        for value in info_file_list_all:
            file.write(value + "\n")
        file.close()

