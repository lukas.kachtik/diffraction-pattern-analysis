from time import time, sleep
import numpy as np
import cv2
import matplotlib.pyplot as plt
from numpy import asarray, copy, sqrt, arctan2, pi, sign
from PIL import Image

from library import Library

# Adjustable constants
scale = 8

central_position_offset_x = -170
central_position_offset_y = 20

radius_min = 1280
radius_max = 1400


image = Image.open('test.tif')
data = asarray(image)[:4096][:]

original_shape = data.shape

data = cv2.resize(data, dsize=(int(original_shape[1] * scale), int(original_shape[0] * scale)),
                  interpolation=cv2.INTER_CUBIC)

start_time = time()

Image.fromarray(data).save('performance_testing_image.tif')

print("Sleeping...")
sleep(5)
data = 1
print("data_variable_deleted")
print("Sleeping")
sleep(5)
print("Time: {}".format(round(time() - start_time, 2)))
