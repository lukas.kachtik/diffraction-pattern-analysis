import time
import multiprocessing
import numpy as np
from PIL import Image
import cv2

class processing:

    def __init__(self, adjustable_constants):

        self.constants = adjustable_constants


    def multiprocessing_func(self, x, y, data, dictionary,n ):

        for i in x:
            # print(i)
            for j in y:
                data[i, j] /= 2
        dictionary[n] = n + 2


    def run(self):
        image = Image.open("test.tif")
        data = np.asarray(image)[:4096][:]
        data = cv2.resize(data, dsize=(4096, 4096), interpolation=cv2.INTER_CUBIC)
        data = np.copy(data)

        starttime = time.time()
        processes = []
        pixels = 4096
        x = list(range(pixels))
        y_list = list()

        threads = 8
        step = int(pixels / threads)

        first = 0
        second = step

        manager = multiprocessing.Manager()

        dictionary = manager.dict()

        for i in range(threads):
            y_list.append(range(first, second))
            first += step
            second += step

        n = 0
        for y in y_list:
            n += 1
            p = multiprocessing.Process(target=self.multiprocessing_func, args=(x, y, data, dictionary, n))
            print(p)
            processes.append(p)
            p.start()


        for process in processes:
            process.join()

        print(dictionary)

        # Image.fromarray(shared_array).save('result.tif')
        print('That took {} seconds'.format(time.time() - starttime))

if __name__ == '__main__':
    obj = processing()
    obj.run()
