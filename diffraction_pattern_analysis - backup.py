import cv2
from numpy import asarray, copy, sqrt, arctan2, pi, sign
from PIL import Image
from sys import stdout
from library import Library


class DiffractionPatternAnalysis:

    def __init__(self):

        class AdjustableConstants:
            def __init__(self):
                self.scale = 1
                self.central_position_offset_x = -170
                self.central_position_offset_y = 20
                self.radius_min = 1280
                self.radius_max = 1400
                self.precision = 3
                self.optimized_speed = False
                self.real_precision = 2 ** self.precision

        self.constants = AdjustableConstants()

    
    def calculate_angle(self, angle):
        return round(round(angle * self.constants.real_precision, 0) / self.constants.real_precision, 2)
    
    def processing(self):
        working_directory = "source_data"
        library = Library(working_directory)
        
        list_of_filenames = library.load_files()
        number_of_proceded_file = 0
        
        for filename in list_of_filenames:
            settings = library.load_settings(filename)
            if settings is not None:
                self.constants.central_position_offset_x = settings[0]
                self.constants.central_position_offset_y = settings[1]
                self.constants.radius_min = settings[2]
                self.constants.radius_max = settings[3]
        
            image = Image.open("{}/{}".format(working_directory, filename))
            image = asarray(image)
            image_shape = image.shape
        
            data = asarray(image)[:image_shape[1]][:]
        
            original_shape = data.shape
        
            data = cv2.resize(data, dsize=(int(original_shape[1] / self.constants.scale), int(original_shape[0] / self.constants.scale)),
                              interpolation=cv2.INTER_CUBIC)
        
            data = copy(data)
            data_2 = copy(data)
            shape = data.shape
        
            result = dict()
            number_of_pixels_in_area = dict()
        
            for i in range(0, 360 * 100):
                i /= 100
                i = round(i, 2)
                result[i] = 0.0
                number_of_pixels_in_area[i] = 0
        
            radius_scaled_min = self.constants.radius_min / self.constants.scale
            radius_scaled_max = self.constants.radius_max / self.constants.scale
        
            if self.constants.optimized_speed:
                horizontal_initial = int(shape[1] / 2 - radius_scaled_max + self.constants.central_position_offset_x / self.constants.scale)
                horizontal_final = int(shape[1] / 2 + radius_scaled_max + self.constants.central_position_offset_x / self.constants.scale) + 1
        
                vertical_initial = int(shape[0] / 2 - radius_scaled_max - self.constants.central_position_offset_y / self.constants.scale)
                vertical_final = int(shape[0] / 2 + radius_scaled_max - self.constants.central_position_offset_y / self.constants.scale) + 1
        
                horizontal_range = list(range(horizontal_initial, horizontal_final))
                vertical_range = list(range(vertical_initial, vertical_final))
            else:
                vertical_range = range(shape[0])
                horizontal_range = range(shape[1])
        
            x_correction = shape[1] / 2 + self.constants.central_position_offset_x / self.constants.scale
            y_correction = shape[0] / 2 - self.constants.central_position_offset_y / self.constants.scale
        
            vertical_length = len(list(vertical_range))
            vertical_range_first_element = vertical_range[0]
        
            number_of_files = len(list_of_filenames)
        
            number_of_proceded_file += 1



            for vertical in vertical_range:
                stdout.write('\r' + "[{}/{}] - {} - {} %".format(number_of_proceded_file, number_of_files, filename,
                                                                 round((vertical - vertical_range_first_element + 1) /
                                                                       vertical_length * 100, 1)))
                for horizontal in horizontal_range:

                    x_distance = horizontal - x_correction
                    y_distance = y_correction - vertical

                    radius = sqrt(x_distance ** 2 + y_distance ** 2)

                    if radius > radius_scaled_min and radius < radius_scaled_max:

                        angle = arctan2(y_distance, x_distance) / pi * 180

                        angle = self.calculate_angle(angle)

                        if sign(angle) == -1:
                            angle += 360
                        if angle == 360:
                            angle = 0

                        angle = round(angle, 2)

                        value = int(data[vertical, horizontal])

                        result[angle] += value
                        number_of_pixels_in_area[angle] += 1

                        angle = round(round(angle * 2 ** self.constants.precision, 0) / (2 ** self.constants.precision),
                                      2)

                        if round(angle % ((1 / 2) ** (self.constants.precision - 1)), 2) == 0:
                            value = data_2[vertical, horizontal]
                            value += 50
                            if value > 255:
                                value = 255
                            data_2[vertical, horizontal] = value
                        else:
                            value = data_2[vertical, horizontal]
                            value -= 40
                            if value < 0:
                                value = 0
                            data_2[vertical, horizontal] = value

                    else:
                        data[vertical, horizontal] /= 2  # decreases the brightness of invalid pixel




            print("")
            result_value_ordered = list()
            result_value_ordered_cumulated = list()
            result_cumulated = dict()
        
            for i in range(0, 60 * 100):
                i /= 100
                i = round(i, 2)
                result_cumulated[i] = 0.0
        
            result_angle_ordered = list()
            number_of_pixels_in_area_ordered = list()
        
            for angle in range(len(result)):
                angle = angle / 100
                angle = round(angle, 2)
        
                value = result[angle] / 1000
        
                if value == 0.0:
                    continue
        
                result_angle_ordered.append(angle)
                result_value_ordered.append(value)
        
                number_of_pixels_in_area_ordered.append(number_of_pixels_in_area[angle])
        
                if angle >= 30 and angle < 330:
                    cumulated_angle = angle % 60
                    cumulated_angle = round(cumulated_angle, 2)
                    result_cumulated[cumulated_angle] += value
        
            result_angle_ordered_cumulated = list()
            for angle in result_cumulated:
                value = result_cumulated[angle]
                if value == 0.0:
                    continue
                result_angle_ordered_cumulated.append(angle)
                result_value_ordered_cumulated.append(value)
        
            library.make_dirs(filename)
            library.save_pictures(self.constants.scale, self.constants.precision, self.constants.real_precision, result_angle_ordered, result_value_ordered,
                                  number_of_pixels_in_area_ordered, result_angle_ordered_cumulated, result_value_ordered_cumulated,
                                  image, data, data_2)



if __name__ == '__main__':
    from time import time
    start_time = time()
    diffraction_pattern_analysis = DiffractionPatternAnalysis()
    diffraction_pattern_analysis.processing()
    print("Time: {} s".format(round(time() - start_time, 2)))
